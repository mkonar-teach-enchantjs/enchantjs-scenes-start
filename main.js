/** Rain Game with welcome screen. **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png', 'usertrash32.png', 'sky.png');

    // Additional game properties (accessible to all helper functions).
    game.score = 0; // Stores player score

    // Specify what should happen when the game loads.
    game.onload = function () {
        // Root scene is now a start screen.
        // Go! button uses the droplet image.
        game.goButton = new Sprite(24, 24);
        game.goButton.image = game.assets['droplet24.png'];
        game.goButton.x = (game.width - game.goButton.width) / 2;
        game.goButton.y = (game.height - game.goButton.height) / 2;
        game.rootScene.addChild(game.goButton);

        game.screenMsg = new Label("Catch as many objects as you can.<br>Click the droplet to start.");
        game.rootScene.addChild(game.screenMsg);

        // Make the scene with the game in it go when droplet clicked.
        game.goButton.addEventListener(Event.TOUCH_END, function (event) {
            game.pushScene(makeRainGameScene());
        });
    };

    // Start the game.
    game.start();

    // ==== myGame helper functions ====================================
    // -----------------------------------------------------------------
    // Rain Game stuff
    // -----------------------------------------------------------------
    /** Make a scene to contain the Rain Game. */
    function makeRainGameScene() {
        var scene = new Scene();

        // Background image.
        var bg = new Sprite(game.width, game.height);
        bg.image = game.assets['sky.png'];
        scene.addChild(bg);

        // This object starts counting down once the game is started.
        scene.timerDown = {
            frameCount: 10 * game.fps, // total needed frames.
            tick: function () { // call tick() every frame.
                if (game.isStarted) {
                    this.frameCount -= 1; // count down instead of up.
                }
            }
        };

        // Create a label to show time. (Placed at 0,0 by default.)
        var timeLabel = new Label("Collect droplets as fast as you can.");
        scene.addChild(timeLabel);

        // Make the collector
        var collector = makeCollector(scene);

        // Add an event listener for each frame.
        scene.addEventListener(Event.ENTER_FRAME, function () {
            if (scene.timerDown.frameCount > 0) { // if the game is on ...
                // Make a droplet every second.
                if (game.frame % 24 === 0) {
                    makeDroplet(collector, scene);
                }

                // Tick the timer.
                scene.timerDown.tick();

                // Update label.
                timeLabel.text = 'Time remaining: ' +
                    Math.ceil(scene.timerDown.frameCount / game.fps);
            } else if (scene.timerDown.frameCount !== -1) { // Game over?
                // indicate that the "game over" by setting frameCount to -1
                scene.timerDown.frameCount = -1;
                // update label
                game.screenMsg.text = 'Time\'s up!<br>Score: ' + game.score;
                // end game by returning to rootScene.
                game.popScene();
            }
        });

        return scene;
    }

    /** Make a droplet with the needed listeners in the specified scene.
     * If droplet collides with collector, remove droplet and give 
     * player a point. If droplet goes off screen, remove it.
     * Return a reference to the created droplet. */
    function makeDroplet(collector, scene) {
        var droplet = new Sprite(24, 24);
        droplet.image = game.assets['droplet24.png'];

        // Add a gravity property to the droplet.
        droplet.gravity = 5; // How fast will the droplet drop?

        // Position droplet in random x and random y above screen.
        droplet.x = randomInt(0, game.width - droplet.width);
        droplet.y = randomInt(-1 * game.height, -1 * droplet.height);

        // Add droplet to scene.
        scene.addChild(droplet);

        // Add an event listener to the droplet Sprite to move it.
        droplet.addEventListener(Event.ENTER_FRAME, function () {
            // Game starts when first drop starts falling.
            game.isStarted = true;

            // Move.
            if (droplet.y > game.height) { // if below canvas, remove.
                scene.removeChild(droplet);
            } else { // else move down as usual.
                droplet.y += droplet.gravity;
            }

            // Check for collision.
            if (droplet.intersect(collector)) {
                scene.removeChild(droplet);
                catchDroplet();
            }
        });

        return droplet;
    }

    /** Make a collector with the needed listeners in the
     * specified scene.
     * Return a reference to the created object. */
    function makeCollector(scene) {
        var collector = new Sprite(32, 32);
        collector.image = game.assets['usertrash32.png'];

        // Add a speed property to the collector.
        collector.speed = 4; // How fast will collector move?

        collector.x = (game.width + collector.width) / 2;
        collector.y = game.height - collector.height;

        // Add collector to scene.
        scene.addChild(collector);

        // Event listeners for collector.
        collector.addEventListener(Event.ENTER_FRAME, function () {
            // Move.
            if (game.input.right && !game.input.left) {
                collector.x += collector.speed;
            } else if (game.input.left && !game.input.right) {
                collector.x -= collector.speed;
            }

            // Check limits.
            if (collector.x > game.width - collector.width) {
                collector.x = game.width - collector.width;
            } else if (collector.x < 0) {
                collector.x = 0;
            }
        });

        return collector;
    }

    /** Give the player a point for catching a drop. */
    function catchDroplet() {
        game.score += 1;
    }

    // -----------------------------------------------------------------
    // General stuff
    // -----------------------------------------------------------------
    /** Generate a random integer between low and high (inclusive). */
    function randomInt(low, high) {
        return low + Math.floor((high + 1 - low) * Math.random());
    }
    // ==== End myGame helper functions ================================
}
